#!/bin/sh


# TODO check idempotency of all functions

# Exit script on failure
set -e

echo "$DIR"
# Load config
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
. $SCRIPT_DIR/config.sh
cd || exit

echo "==> Set timezone"
ln -sf "/usr/share/zoneinfo/$TIMEZONE_LOCATION" /etc/localtime

echo "==> Sync hardware clock"
hwclock --systohc

echo "==> Update pacman package list"
pacman -Sy --needed --noconfirm

if [ "$USE_REFLECTOR" = true ]; then
  echo "==> Install reflector and update mirrorlist"
  pacman -S --needed --noconfirm reflector
  reflector --verbose --latest 5 --sort rate --save /etc/pacman.d/mirrorlist
fi

echo "==> Uncomment en_US.UTF-8 from /etc/locale.gen if commented"
sed -i 's/\#en\_US\.UTF\-8\ UTF\-8/en\_US\.UTF\-8\ UTF\-8/g' /etc/locale.gen

echo "==> Create /etc/locale.conf"
{
  echo "LANG=en_US.UTF-8"
  echo "LC_CTYPE=en_US.UTF-8"
  echo "LC_TYPE=en_US.UTF-8"
  echo "LC_MESSAGES=en_US.UTF-8"
  echo "LC_ALL=en_US.UTF-8"
  echo "LANGUAGE=\"en_US.UTF-8\""
} > /etc/locale.conf

echo "==> Generate locale"
locale-gen

echo "==> Set hostname"
# shellcheck disable=SC2039
echo "$NEW_HOSTNAME" > /etc/hostname

echo "==> Set hosts"
FILE='/etc/hosts'
declare -a LINES=(
  "127.0.0.1 localhost.localdomain localhost"
  "::1 localhost.localdomain localhost"
  "127.0.0.1 $NEW_HOSTNAME.localdomain $NEW_HOSTNAME"
)
for LINE in "${LINES[@]}"
do
  grep -qF -- "$LINE" "$FILE" || echo "$LINE" >> "$FILE"
done

if [ "$ENABLE_ENCRYPTION" = true ]; then
  echo "Modify mkinitcpio for luks"
  # TODO not tested for idempotency
  pacman -S --needed --noconfirm mkinitcpio
  sed -i 's/MODULES=()/MODULES=(ext4 vfat)/g' /etc/mkinitcpio.conf
  sed -i 's/HOOKS=(base udev autodetect modconf block filesystems keyboard fsck)/HOOKS=(base udev block keyboard autodetect modconf encrypt filesystems fsck)/g' /etc/mkinitcpio.conf
fi

echo "==> Genereate initial ramdisk image"
pacman -S --needed --noconfirm linux linux-firmware

if [ "$UEFI" = true ]; then
  echo "==> Setting up systemd boot"
  # TODO setup systemd-boot
  bootctl install
  bootctl update

  echo "default arch" > /boot/loader/loader.conf
  FILE='/boot/loader/entries/arch.conf'

  declare -a LINES=(
    "title $NEW_HOSTNAME"
    "linux /vmlinuz-linux"
    "initrd /initramfs-linux.img"
    "options root=UUID=$(blkid -s UUID -o value /dev/${DISK_TO_PARTITION}${ROOT_PARTITION_NUMBER}) rw"
  )
  for LINE in "${LINES[@]}"
  do
    grep -qF -- "$LINE" "$FILE" || echo "$LINE" >> "$FILE"
  done
else
  echo "==> Setup Grub"
  # TODO need to check for idempotency
  pacman -S --needed --noconfirm grub
  grub-install --target=i386-pc --recheck "/dev/$DISK_TO_PARTITION"
  if [ "$ENABLE_ENCRYPTION" = true ]; then
    sed -i "s/GRUB_CMDLINE_LINUX=\"\"/GRUB_CMDLINE_LINUX=\"cryptdevice=\/dev\/${DISK_TO_PARTITION}${ROOT_PARTITION_NUMBER}\:$ENCRYPTED_DEVICE_NAME\:allow\-discards\"/g" /etc/default/grub
  fi
  grub-mkconfig -o /boot/grub/grub.cfg
fi

echo "==> Install necessary packages"
pacman -S --needed --noconfirm \
  iproute2 \
  iw \
  networkmanager \
  openssh \
  python3 \
  wpa_supplicant


echo "==> Setup SSH keys"
mkdir -p ~/.ssh
curl -L "$PUBLIC_KEYS_LINK" > ~/.ssh/authorized_keys
echo "==> Enable SSH"
systemctl enable sshd

echo "==> Enable networking"
pacman -S --needed --noconfirm dhcpcd
systemctl enable dhcpcd

echo "==> Modify pacman config"
sed -i 's/\#Color/Color/g' /etc/pacman.conf
sed -i 's/\#TotalDownload/TotalDownload/g' /etc/pacman.conf
sed -i 's/\#VerbosePkgLists/VerbosePkgLists/g' /etc/pacman.conf
sed -i '/VerbosePkgLists/a ILoveCandy' /etc/pacman.conf

echo "==> Install other packages"
pacman -S --needed --noconfirm archlinux-keyring

echo "==> Create personal user '$PERSONAL_USER' if not exists"
id -u $PERSONAL_USER &>/dev/null || useradd -m -G wheel -s /bin/bash "$PERSONAL_USER"
echo "==> Setup authorized_keys for personal user"
runuser -l "$PERSONAL_USER" -c "mkdir -p ~/.ssh"
runuser -l "$PERSONAL_USER" -c "curl -L \"$PUBLIC_KEYS_LINK\" > ~/.ssh/authorized_keys"

if [ "$PASSWORDLESS_SUDO" = true ]; then
  echo "==> Allow passwordless sudo access for personal user"
  FILE='/etc/sudoers'
  LINE="$PERSONAL_USER ALL=(ALL:ALL) NOPASSWD:ALL"
  grep -qF -- "$LINE" "$FILE" || echo "$LINE" | EDITOR='tee -a' visudo
fi

echo "==> Allow sudo access to users in wheel group"
echo "%wheel ALL=(ALL) ALL" > /etc/sudoers.d/0-wheel-sudo
echo "==> Set password for personal user"
printf "%s\n%s\n" "$PERSONAL_USER_PASSWORD" "$PERSONAL_USER_PASSWORD" | passwd "$PERSONAL_USER"

echo "==> Clean up the machine id"
echo "" > /etc/machine-id

exit 0

