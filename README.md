<div align="center">
    <img src="./.assets/logo.png" width="250">
    <h1>
        CARBS
    </h1>
    <h4>Coding_Coffee's Auto Rice Bootstraping Scripts</h4>
</div>


## Important

**This project is still in alpha release state. Things may break. Consider
yourself warned.**


## Overview

A bunch of scripts to auto configure and setup a minimal Arch
Linux system, which can serve as a base for a server or a Desktop alike.
Originally the intention was to use these shell scripts to RICE my system,
however, since it didn't scale well, I've fallen back to a simple minimal
install.

|-------------|------------------------------|
| Options     | Possible Options             |
|-------------|------------------------------|
| UEFI        | UEFI / Legacy                |
| Secure Boot | Yes / No                     |
| Bootloader  | rEFInd / systemd-boot / grub |
| LVM         | Yes / No                     |
| LUKS        | Yes / No                     |
|-------------+------------------------------|


## Use case

To have a super minimal arch install up and running in no time.

A minimal install is one with only the components installed which are required
to boot up an OS with the following capabilities:

1. Network connectivity - To further build on the minimal install
2. Partitioning - Since it is at this stage that if partitioning is not done
   right we run into problems later.
3. UEFI considerations - Since the install and 1st boot is not possible without
   it.
4. Timezone - UTC
5. Personal User - Although security is not the focus of a minimal install,
   going by the rule of thumb, this is beneficial for all types of scenarios,
   hence it qualifies

And the following optional capabilities:

1. SSH connectivity - Since we may need to build a remote server
2. Encryption - Again by the same argument, if we don't have encryption, adding
   it at a later stage becomes a hassle, since we prefer LVM on LUKS.

The following things are **not** the target of a minimal install:

1. Processor specific microcode - doesn't play well with taking snapshots

A simple rule of thumb which can be followed while thinking about a feature /
package addition is if the package will be usefull for a Kubernetes, Desktop,
and a Traditional server alike. Also will it be usefull on all types of CPU
architectures.


## Usage

### For setting up a PC

- Download Arch Linux ISO from [here](https://www.archlinux.org/download/)
- Burn it on a USB drive
```sh
sudo dd if={path_to_iso} of={path_to_usb_device} status=progress && sync
```
Example:
```sh
sudo dd if=archlinux-2020.07.01-x86_64.iso of=/dev/sdb status=progress && sync
```
- Boot the PC using the USB
- Connect to WiFi or use Ethernet
You can know your interface using `ip addr` for wireless it'll usually be starting with `wlan` or `wlp`
```sh
ip link set {interface} up
wpa_supplicant -B -i {interface} -c <(wpa_passphrase {MYSSID} {passphrase})
```

### On a remote server

#### Hetzner

- Create a server with your OS of choice.
- Mount the Arch Linux ISO
- Restart the server, so that it boots into Arch Linux. At this point, you
  won't be able to login to the server, since it has booted via the Arch Live
  ISO. Hence you'll have to use Hetzner's web console to access the server.
- Once you have access, execute the following commands

### Common steps

- Enable SSH to connect to your PC / server (optional)
```
systemctl enable --now sshd
mkdir ~/.ssh
curl https://github.com/codingcoffee.keys > ~/.ssh/authorized_keys
```

- Install git and clone carbs
```sh
pacman -Sy --noconfirm glibc git
git clone http://gitlab.com/codingCoffee/carbs
cd carbs
cp example.config.sh config.sh
```

- Now modify the `config.sh` as per your choice
- Finally initialize the installation

```sh
./carbs.sh
```

- Done! You have a minimal installation of Arch Linux. You may now poweroff your
  server. Unmount the Arch ISO and start your server again. If you have opted
  for encryption, you'll need to login through the web console to be able to
  enter your encryption password to decrypt the drive everytime you reboot. This
  time when you SSH into the server, you'll inside your freshly installed Arch
  Linux.


## FAQ

- To swap escapse and capslock in tty

```sh
sudo loadkeys files/.keystrings
```


## ToDo

- [ ] Storge - make a case of manual partitioning with a supplimentary script
  - [ ] Partition existing, and not to format
  - [ ] Partition existing, and to format
  - [ ] Different types of partitioning
- [ ] Use this for better boot partition management https://ramsdenj.com/2016/04/15/multi-boot-linux-with-one-boot-partition.html

