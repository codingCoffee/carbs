#!/bin/sh

# Configurable variables
export NEW_HOSTNAME="interim"
export DISK_TO_PARTITION="sda"
export PUBLIC_KEYS_LINK="https://github.com/codingcoffee.keys"
export TIMEZONE_LOCATION="Asia/Kolkata"
export PERSONAL_USER="newuser"
export PASSWORDLESS_SUDO=false
export PERSONAL_USER_PASSWORD="password"
export ENCRYPTION_PASSWORD="password"

# Things which need not be configured
export ENCRYPTED_DEVICE_NAME="lukspart"
export UEFI=true
export ENABLE_ENCRYPTION=true
export USE_REFLECTOR=true

