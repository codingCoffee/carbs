#!/bin/sh


# Exit script on failure
set -e


# Import custom configurations
# shellcheck disable=SC1091
. ./config.sh


echo_warn_exit(){
  echo "=== Please read the instructions before using the script!"
  exit
}

test_if_in_archiso(){
  echo "==> Testing if executing script in arch liveboot environment"
  lsblk | grep loop0 | grep archiso > /dev/null || echo_warn_exit
}

pre_install_warning(){
  while true; do
    echo "This will erase all the data and install Arch Linux on your server. Are you absolutely sure you want to contine? (y/n)? "
    old_stty_cfg=$(stty -g)
    stty raw -echo ; answer=$(head -c 1) ; stty "$old_stty_cfg" # Careful playing with stty

    case $answer in
      [Yy]* ) break;;
      [Nn]* ) exit;;
      * ) echo "Please answer yes or no.";;
    esac
  done
}

test_custom_configs(){
  # TODO test if luks/lvm/uefi table is satisfied
  echo "==> Confirming presense of all required variables"
  for VARIABLE in NEW_HOSTNAME DISK_TO_PARTITION PUBLIC_KEYS_LINK TIMEZONE_LOCATION PERSONAL_USER ENCRYPTION_PASSWORD ENCRYPTED_DEVICE_NAME
  do
    eval VAR=\"\$$VARIABLE\"
    if [ -z "$VAR" ]; then
      echo "The variable $VARIABLE is not set. Please read the instructions before using the script";
      exit
    fi
  done
}

test_internet(){
  echo "==> Testing internet connection..."
  if ping -q -c 1 -W 1 1.1.1.1 > /dev/null; then
    echo "==> Internet is working"
  else
    echo "=== Internet connection is down. Please fix the same and try again."
    exit
  fi
}

test_uefi(){
  echo "==> Checking if running in UEFI mode or Legacy mode"
  if [ -d "/sys/firmware/efi/efivars" ]; then
    if [ ! "$UEFI" = true ]; then
      echo "UEFI not set correctly in config, aborting!"
      echo_warn_exit
    fi
    echo "==> Running in UEFI mode"
  else
    if [ "$UEFI" = true ]; then
      echo "UEFI not set correctly in config, aborting!"
      echo_warn_exit
    fi
    echo "==> Running in legacy mode"
  fi
}

set_time(){
  echo "==> Setting system time using ntp"
  timedatectl set-ntp true
}

# Partition and format disk
partition_and_format_disk(){
  echo "==> Partitioning disk"
  # TODO add aditional option to manually partition disk and specify only variables
  # TODO add option for btrfs and ext4

  # +---------------------------+--------+
  # + Combination               + Status |
  # +---------------------------+--------+
  # | LVM     + LUKS   + UEFI   | TODO   |
  # | Non LVM + LUKS   + UEFI   | TODO   |
  # | LVM     + Normal + UEFI   | TODO   |
  # | Non LVM + Normal + UEFI   | Done   |
  # | LVM     + LUKS   + Legacy | TODO   |
  # | Non LVM + LUKS   + Legacy | TODO   |
  # | LVM     + Normal + Legacy | TODO   |
  # | Non LVM + Normal + Legacy | Done   |
  # +---------------------------+--------+

  if [ "$UEFI" = true ]; then
    # Laptops mostly
    # TODO Dont re partition device if already done, have a .status file to track changes
    echo "==> Partitioning for UEFI system using sgdisk"

    echo "===> Creating fresh GPT table"
    sgdisk -og "/dev/${DISK_TO_PARTITION}"

    echo "===> Creating boot partition of 512 MB"
    sgdisk -n 0:0:+512MiB -t 0:ef00 -c 0:"Boot partition" "/dev/${DISK_TO_PARTITION}"

    echo "===> Creating root partition of 200 GB"
    sgdisk -n 0:0:+200GiB -t 0:8303 -c 0:"Root Disk" "/dev/${DISK_TO_PARTITION}"

    echo "===> Creating a storage partition"
    sgdisk -n 0:0:0 -t 0:8300 -c 0:"Storage Partition" "/dev/${DISK_TO_PARTITION}"

    echo "===> Writing to disk"
    sgdisk -p "/dev/${DISK_TO_PARTITION}"

    echo "==> Formatting disk"
    # TODO combine both formatting disk
    mkfs.vfat -F32 "/dev/${DISK_TO_PARTITION}1"
    mkfs.ext4 "/dev/${DISK_TO_PARTITION}2"
    mkfs.ext4 "/dev/${DISK_TO_PARTITION}3"

  else
    # Servers mostly
    echo "==> Partitioning for non UEFI system using sfdisk"
    sfdisk "/dev/${DISK_TO_PARTITION}" < partition.sfdisk
    # Format boot as vfat
    echo "==> Formatting disk"
    mkfs.vfat -F32 "/dev/${DISK_TO_PARTITION}1"

    if [ "$ENABLE_ENCRYPTION" = true ]; then
      # Check if dm-crypt is loaded
      modprobe dm-crypt
      # Setup LUKS partition
      echo "$ENCRYPTION_PASSWORD" | cryptsetup --cipher aes-xts-plain64 --key-size 512 --hash sha512 --iter-time 2000 --use-random luksFormat "/dev/${DISK_TO_PARTITION}2"
      echo "$ENCRYPTION_PASSWORD" | cryptsetup open /dev/${DISK_TO_PARTITION}2 "$ENCRYPTED_DEVICE_NAME"
      # Format lukspart as ext4
      mkfs.ext4 "/dev/mapper/$ENCRYPTED_DEVICE_NAME"
    else
      mkfs.ext4 "/dev/${DISK_TO_PARTITION}2"
    fi
  fi
}

# Mount disk for Arch Linux installation
mount_disk(){
  echo "==> Mounting partitions"
  if [ "$ENABLE_ENCRYPTION" = true ]; then
    mount "/dev/mapper/$ENCRYPTED_DEVICE_NAME" /mnt
  else
    mount "/dev/${DISK_TO_PARTITION}2" /mnt
  fi
  mkdir -p /mnt/boot
  mount "/dev/${DISK_TO_PARTITION}1" /mnt/boot
}

# Setup Arch
setup_arch(){
  if [ "$USE_REFLECTOR" = true ]; then
    echo "==> Install reflector and update mirrorlist"
    pacman -S --needed --noconfirm reflector
    reflector --verbose --latest 5 --sort rate --save /etc/pacman.d/mirrorlist
  fi

  echo "==> Modify pacman config for better visibility"
  sed -i 's/\#Color/Color/g' /etc/pacman.conf
  sed -i 's/\#TotalDownload/TotalDownload/g' /etc/pacman.conf
  sed -i 's/\#VerbosePkgLists/VerbosePkgLists/g' /etc/pacman.conf
  sed -i '/VerbosePkgLists/a ILoveCandy' /etc/pacman.conf

  echo "==> Strap Arch base image"
  pacstrap /mnt base base-devel

  echo "==> Generate fstab"
  genfstab -pU /mnt >> /mnt/etc/fstab

  echo "==> Chroot into the drive to setup Arch"
  cp -r "$(pwd)" /mnt/root/.
  arch-chroot /mnt /root/carbs/chroot.sh

  echo "==> Unmount the drive"
  umount -R /mnt
  if [ "$ENABLE_ENCRYPTION" = true ]; then
    echo "==> Closing luks partition"
    cryptsetup close "$ENCRYPTED_DEVICE_NAME"
  fi
}


###### MAIN SCRIPT ######

test_if_in_archiso
pre_install_warning
test_custom_configs
test_internet
test_uefi
set_time
partition_and_format_disk
mount_disk
setup_arch

echo "==> Installation complete"

#########################

echo "Installation of Arch Linux complete. You may the now poweroff your server, unmount the ARCHISO and start your server again!"
